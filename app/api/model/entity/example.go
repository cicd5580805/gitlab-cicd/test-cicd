package entity

import "time"

// TestUser TestUser
type TestUser struct {
	ID        int       `gorm:"primary_key;auto_increment" json:"id"`
	UserName  string    `gorm:"column:user_name;type:varchar(255);not null;comment:'用户名';" json:"user_name"`
	CreatedAt time.Time `gorm:"column:created_at;type:datetime;not null;comment:'创建时间';" json:"created_at"`
	UpdatedAt time.Time `gorm:"column:updated_at;type:datetime;not null;comment:'修改时间';" json:"updated_at"`
}

// TableName TableName
func (p *TestUser) TableName() string {
	return "test_user"
}

package controller

import (
	"github.com/gin-gonic/gin"
	"test_cicd/app/api/utils/response"

	"test_cicd/app/api/handler"
)

// Hello Hello
func Hello(c *gin.Context) {

	err := handler.Hello()
	if err != nil {
		response.InternalServerError(c, err.Error())
	}

	err = handler.Hello2()
	if err != nil {
		response.InternalServerError(c, err.Error())
	}

	err = handler.Hello3()
	if err != nil {
		response.InternalServerError(c, err.Error())
	}

	response.OK(c, "hello")
}

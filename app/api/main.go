package main

import (
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"test_cicd/app/api/controller"
	"test_cicd/app/api/store"
	"test_cicd/app/api/store/redis"
	"test_cicd/app/api/utils/config"

	"github.com/gin-gonic/gin"
	"pkg.deepin.com/service/lib/log"
)

func main() {

	// 加载配置
	err := config.InitConfig()
	if err != nil {
		log.Panic("InitConfig", log.FieldErr(err))
	}

	// 初始化日志
	logConfig, err := config.Log()
	if err != nil {
		log.Panic("UnmarshallLogConfig", log.FieldErr(err))
	}
	log.InitLogger(*logConfig)

	// 初始化db
	err = store.OpenDB()
	if err != nil {
		log.Panic("openDB", log.FieldErr(err))
	}

	// 初始化redis
	err = redis.OpenRedis()
	if err != nil {
		log.Panic("OpenRedis", log.FieldErr(err))
	}

	// 初始化框架&&注册路由
	server := gin.Default()
	controller.InitRouter(server)

	httpConf := config.Server()
	srv := &http.Server{
		Addr:    httpConf.Address,
		Handler: server,
	}
	// 启动 http服务
	go func() {
		err = srv.ListenAndServe()
		if err != nil {
			log.Panic("serverRunErr", log.FieldErr(err))
		}
	}()

	waitForSignal()

}

func waitForSignal() os.Signal {
	signalChan := make(chan os.Signal, 1)
	defer close(signalChan)
	signal.Notify(signalChan, os.Kill, syscall.SIGTERM, os.Interrupt)
	s := <-signalChan
	signal.Stop(signalChan)
	return s
}

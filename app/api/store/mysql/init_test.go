package mysql

import (
	"fmt"
	l "log"
	"test_cicd/app/api/model/entity"
	"test_cicd/app/api/store"
	"test_cicd/app/api/utils/config"
)

// 本地测试时，使用以下命令运行mysql容器，并在/etc/hosts内增加127.0.0.1 mysql
// docker run --rm -p 3306:3306 -e MARIADB_DATABASE=udcp_test -e MARIADB_ROOT_PASSWORD=Udcp2022cs --name mysql hub.deepin.com/library/mariadb:10.3.35
// 停止运行mysql容器
// docker stop mysql

const (
	testDatabase = "udcp_test"
)

// // init init
// func init() {
//
// 	l.SetFlags(l.Llongfile | l.LstdFlags)
// 	l.Println("init()")
//
// 	InitTest()
// }

// InitTest InitTest
func InitTest() {
	// 加载配置
	err := config.InitConfig()
	if err != nil {
		l.Fatal(err)
	}

	// 初始化db
	err = store.OpenDB()
	if err != nil {
		l.Fatal(err)
	}

	// 创建测试数据库
	err = createTestDatabase()
	if err != nil {
		l.Fatal(err)
	}
	// 创建表
	err = autoCreateTable()
	if err != nil {
		l.Fatal(err)
	}
}

// CreateDatabase 创建数据库
func CreateDatabase(db string) error {

	return store.MasterDB().Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s default charset utf8", db)).Error
}

// DropDatabase 删除数据库
func DropDatabase(db string) error {

	return store.MasterDB().Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %s", db)).Error
}

// autoCreateTable 自动创建表，使用gorm特性，建议仅用于测试
func autoCreateTable() error {

	models := []interface{}{
		&entity.TestUser{},
	}

	return store.MasterDB().Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(models...).Error
}

// createTestDatabase 创建测试数据库
func createTestDatabase() error {
	return CreateDatabase(testDatabase)
}

// dropTestDatabase 删除测试数据库
func dropTestDatabase() error {
	return DropDatabase(testDatabase)
}

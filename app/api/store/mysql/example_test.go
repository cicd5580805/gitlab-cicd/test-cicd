package mysql

import (
	"fmt"
	. "github.com/agiledragon/gomonkey/v2"
	"github.com/jinzhu/gorm"
	. "github.com/smartystreets/goconvey/convey"
	"test_cicd/app/api/model/entity"
	"testing"
)

// 不依赖数据库

// TestHasTestUserByID TestHasTestUserByID
func TestHasTestUserByID(t *testing.T) {

	{
		Convey("TestHasTestUserByID", t, func() {

			Convey("fail", func() {

				// 打桩，模拟返回
				patch := ApplyFunc(GetTestUserByID, func(_ int) (m entity.TestUser, err error) {
					err = fmt.Errorf("网络错误")
					return m, err
				})
				defer patch.Reset()

				id := 1
				_, _, err := HasTestUserByID(id)
				So(err, ShouldNotBeNil)
			})

			Convey("not found", func() {

				// 打桩，模拟返回
				patch := ApplyFunc(GetTestUserByID, func(_ int) (m entity.TestUser, err error) {
					err = gorm.ErrRecordNotFound
					return m, err
				})
				defer patch.Reset()

				id := 1
				has, _, err := HasTestUserByID(id)
				So(err, ShouldBeNil)
				So(has, ShouldBeFalse)
			})

			Convey("success", func() {

				// 打桩，模拟返回
				patch := ApplyFunc(GetTestUserByID, func(_ int) (m entity.TestUser, err error) {
					return m, nil
				})
				defer patch.Reset()

				id := 1
				_, _, err := HasTestUserByID(id)
				So(err, ShouldBeNil)
			})
		})
	}
}

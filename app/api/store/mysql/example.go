package mysql

import (
	"github.com/jinzhu/gorm"
	"test_cicd/app/api/model/entity"
	"test_cicd/app/api/store"
)

// GetTestUserByID 根据Id查询用户
func GetTestUserByID(id int) (model entity.TestUser, err error) {
	err = store.SlaveDB().Model(entity.TestUser{}).First(&model, id).Error
	return
}

// HasTestUserByID 不存在时不报错
func HasTestUserByID(id int) (has bool, m entity.TestUser, err error) {
	m, err = GetTestUserByID(id)
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, m, nil
		}
		return false, m, err
	}
	return true, m, nil
}

// CreateTestUser 创建用户
func CreateTestUser(id int, userName string) (err error) {
	m := entity.TestUser{
		ID:       id,
		UserName: userName,
	}
	err = store.MasterDB().Model(&entity.TestUser{}).Create(&m).Error
	return
}

// CreateTestUserByUserName 创建用户
func CreateTestUserByUserName(userName string) (err error) {
	m := entity.TestUser{
		UserName: userName,
	}
	err = store.MasterDB().Model(&entity.TestUser{}).Create(&m).Error
	return
}

// DeleteTestUserByID 根据Id删除用户
func DeleteTestUserByID(id int) (err error) {
	err = store.MasterDB().Model(entity.TestUser{}).Where("id = ?", id).Delete(&entity.TestUser{}).Error
	return
}

// DeleteTestUserByUserName 根据userName删除用户
func DeleteTestUserByUserName(userName string) (err error) {
	err = store.MasterDB().Model(entity.TestUser{}).Where("user_name = ?", userName).Delete(&entity.TestUser{}).Error
	return
}

package store

import (
	"test_cicd/app/api/utils/config"

	"pkg.deepin.com/service/lib/storage/db/v8"
)

// OpenDB 数据库初始化
func OpenDB() (err error) {
	conf, err := config.DB()
	db.InitManager(conf)

	return
}

// MasterDB 主库
func MasterDB() *db.Conn {
	c := db.Master("udcp_test")
	c.SingularTable(true)
	return c
}

// SlaveDB 从库
func SlaveDB() *db.Conn {
	c := db.Slave("udcp_test")
	c.SingularTable(true)
	return c
}

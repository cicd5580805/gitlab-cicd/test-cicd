package redis

import (
	"pkg.deepin.com/service/lib/storage/rediser/v8"
	"test_cicd/app/api/utils/config"
)

// OpenRedis OpenRedis
func OpenRedis() (err error) {
	conf, err := config.Redis()
	if err != nil {
		return err
	}
	rediser.InitManager(conf)
	return
}

// MasterRedis MasterRedis
func MasterRedis() *rediser.Conn {
	c := rediser.Master("udcp_test")
	return c
}

// SlaveRedis SlaveRedis
func SlaveRedis() *rediser.Conn {
	c := rediser.Slave("udcp_test")
	return c
}

package redis

import (
	l "log"
	"test_cicd/app/api/utils/config"
)

// 本地测试时，使用以下命令运行redis容器，并在/etc/hosts内增加127.0.0.1 redis
// docker run --rm -p 6379:6379 --name redis hub.deepin.com/wuhan_udcp/redis:5.0.9 --requirepass Udcp2022cs
// 停止运行redis容器
// docker stop redis

// // init init
// func init() {
//
// 	l.SetFlags(l.Llongfile | l.LstdFlags)
// 	l.Println("init()")
//
// 	InitTest()
// }

// InitTest InitTest
func InitTest() {
	// 加载配置
	err := config.InitConfig()
	if err != nil {
		l.Fatal(err)
	}

	// 初始化redis
	err = OpenRedis()
	if err != nil {
		l.Fatal(err)
	}

}

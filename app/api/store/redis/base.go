package redis

import (
	"time"

	"github.com/go-redis/redis"
)

// Get  redis根据key获取value
// Get Get key不存在时，返回空字符串，且error=nil
func Get(key string) (string, error) {
	v, err := SlaveRedis().Get(key).Result()
	if err == redis.Nil {
		// key不存在时，v="", err = redis: nil
		return v, nil
	} else if err != nil {
		return v, err
	}
	return v, nil
}

// Set Set
func Set(key string, value string) error {
	_, err := MasterRedis().Set(key, value, 0).Result()
	if err != nil {
		return err
	}

	return nil
}

// SetExp SetExp
func SetExp(key string, value string, exp time.Duration) error {
	_, err := MasterRedis().Set(key, value, exp).Result()
	if err != nil {
		return err
	}

	return nil
}

// Del Del
func Del(key string) error {
	_, err := MasterRedis().Del(key).Result()
	if err != nil {
		return err
	}

	return nil
}

/*
SetEX SetEX
相当于 SET key value + EXPIRE key seconds  # 设置key生存时间
*/
func SetEX(key string, value string, expire int64) error {
	_, err := MasterRedis().Set(key, value, time.Duration(expire)*time.Second).Result()
	if err != nil {
		return err
	}

	return nil
}

/*
SetExpireAt SetExpireAt
相当于 SET key value + EXPIREAT key 1355292000		# 设置key在指定时刻(utc时间戳)过期
*/
func SetExpireAt(key string, value string, expireAt int64) error {
	_, err := MasterRedis().Set(key, value, 0).Result()
	if err != nil {
		return err
	}

	_, err = MasterRedis().ExpireAt(key, time.Unix(expireAt, 0)).Result()
	if err != nil {
		return err
	}

	return nil
}

// SAddOne SAddOne
func SAddOne(key string, member interface{}) error {
	_, err := MasterRedis().SAdd(key, member).Result()
	if err != nil {
		return err
	}
	return nil
}

// SAdd SAdd
func SAdd(key string, members ...interface{}) error {
	_, err := MasterRedis().SAdd(key, members).Result()
	if err != nil {
		return err
	}
	return nil
}

// SRemOne SRemOne
func SRemOne(key string, member interface{}) error {
	_, err := MasterRedis().SRem(key, member).Result()
	if err != nil {
		return err
	}
	return nil
}

// SRem SRem
func SRem(key string, members ...interface{}) error {
	_, err := MasterRedis().SRem(key, members).Result()
	if err != nil {
		return err
	}
	return nil
}

// SIsMember SIsMember
func SIsMember(key string, member interface{}) error {
	_, err := SlaveRedis().SIsMember(key, member).Result()
	if err != nil {
		return err
	}
	return nil
}

// HSet HSet
func HSet(key, field string, value interface{}) error {
	_, err := MasterRedis().HSet(key, field, value).Result()
	if err != nil {
		return err
	}
	return nil
}

// HDel HDel
func HDel(key string, field string) error {
	_, err := MasterRedis().HDel(key, field).Result()
	if err != nil {
		return err
	}
	return nil
}

// HExists HExists
func HExists(key, field string) error {
	_, err := SlaveRedis().HExists(key, field).Result()
	if err != nil {
		return err
	}
	return nil
}

// HGet HGet redis根据key、field获取value
// 不存在时，返回空字符串，且error=nil
func HGet(key, field string) (string, error) {
	v, err := SlaveRedis().HGet(key, field).Result()
	if err == redis.Nil {
		//不存在时，v="", err = redis: nil
		return "", nil
	} else if err != nil {
		return "", err
	}
	return v, nil
}

// LPush LPush
func LPush(key string, value string) (err error) {
	_, err = MasterRedis().LPush(key, value).Result()
	if err != nil {
		return
	}
	return
}

// LLen LLen
func LLen(key string) (lens int64, err error) {
	lens, err = SlaveRedis().LLen(key).Result()
	if err != nil {
		return
	}
	return
}

// RPush RPush
func RPush(key string, value string) (err error) {
	_, err = MasterRedis().RPush(key, value).Result()
	if err != nil {
		return
	}
	return
}

// BRPop BRPop
func BRPop(key string) (string, error) {
	data, err := MasterRedis().BRPop(0*time.Millisecond, key).Result()
	if err != nil {
		return "", err
	}
	return data[1], nil
}

package redis

//
// import (
// 	. "github.com/smartystreets/goconvey/convey"
// 	"testing"
// 	"time"
// )
//
// // 依赖redis
// // TestGet
// func TestGet(t *testing.T) {
// 	{
// 		Convey("TestGet", t, func() {
//
// 			// 创建测试数据
// 			key := "test_key"
// 			value := "abc"
// 			err := Set(key, value)
// 			So(err, ShouldBeNil)
//
// 			// 清理测试数据
// 			defer func() {
// 				err = Del(key)
// 				So(err, ShouldBeNil)
// 			}()
//
// 			Convey("success", func() {
//
// 				_, err = Get(key)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
// // TestSet
// func TestSet(t *testing.T) {
// 	{
// 		Convey("TestSet", t, func() {
//
// 			var err error
// 			key := "test_key"
// 			value := "abc"
//
// 			// 清理测试数据
// 			defer func() {
// 				err = Del(key)
// 				So(err, ShouldBeNil)
// 			}()
//
// 			Convey("success", func() {
//
// 				err = Set(key, value)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
// // TestDel
// func TestDel(t *testing.T) {
// 	{
// 		Convey("TestDel", t, func() {
//
// 			var err error
// 			key := "test_key"
// 			value := "abc"
//
// 			err = Set(key, value)
// 			So(err, ShouldBeNil)
//
// 			Convey("success", func() {
//
// 				err = Del(key)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
// func TestSetExp(t *testing.T) {
//
// 	{
// 		Convey("TestSetExp", t, func() {
//
// 			var err error
// 			key := "test_key"
// 			value := "abc"
// 			exp := 10 * time.Second
//
// 			Convey("success", func() {
//
// 				err = SetExp(key, value, exp)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
// func TestSetEX(t *testing.T) {
//
// 	{
// 		Convey("TestSetEX", t, func() {
//
// 			var err error
// 			key := "test_key"
// 			value := "abc"
// 			exp := int64(10)
//
// 			Convey("success", func() {
//
// 				err = SetEX(key, value, exp)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
// func TestSetExpireAt(t *testing.T) {
//
// 	{
// 		Convey("TestSetExpireAt", t, func() {
//
// 			var err error
// 			key := "test_key"
// 			value := "abc"
// 			exp := time.Now().Unix() + int64(10)
//
// 			Convey("success", func() {
//
// 				err = SetExpireAt(key, value, exp)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
// func TestSAddOne(t *testing.T) {
//
// 	{
// 		Convey("TestSAddOne", t, func() {
//
// 			var err error
// 			key := "test_key_set"
// 			value := "abc"
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = SAddOne(key, value)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
//
// // type strSlice struct {
// // 	Value string
// // }
// // func (m strSlice) MarshalBinary() ([]byte, error) {
// // 	return json.Marshal(m)
// // }
// // func TestSAdd(t *testing.T) {
// // 	{
// // 		Convey("TestSAdd", t, func() {
// //
// // 			var err error
// // 			key := "test_key_set"
// //
// // 			v := []strSlice{
// // 				{Value: "abc"},
// // 				{Value: "123"},
// // 			}
// //
// // 			Convey("success", func() {
// //
// // 				// 清理测试数据
// // 				defer func() {
// // 					err = Del(key)
// // 					So(err, ShouldBeNil)
// // 				}()
// //
// // 				err = SAdd(key, v)
// // 				So(err, ShouldBeNil)
// // 			})
// // 		})
// // 	}
// // }
//
// func TestRemOne(t *testing.T) {
//
// 	{
// 		Convey("TestRemOne", t, func() {
//
// 			var err error
// 			key := "test_key_set"
// 			value := "abc"
//
// 			err = SAddOne(key, value)
// 			So(err, ShouldBeNil)
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = SRemOne(key, value)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
//
// func TestSIsMember(t *testing.T) {
//
// 	{
// 		Convey("TestSIsMember", t, func() {
//
// 			var err error
// 			key := "test_key_set"
// 			value := "abc"
//
// 			err = SAddOne(key, value)
// 			So(err, ShouldBeNil)
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = SIsMember(key, value)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
//
// func TestHSet(t *testing.T) {
//
// 	{
// 		Convey("TestHSet", t, func() {
//
// 			var err error
// 			key := "test_key_hash"
// 			field := "f"
// 			value := "abc"
//
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = HSet(key, field, value)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
// func TestHDel(t *testing.T) {
//
// 	{
// 		Convey("TestHDel", t, func() {
//
// 			var err error
// 			key := "test_key_hash"
// 			field := "f"
// 			value := "abc"
//
// 			err = HSet(key, field, value)
// 			So(err, ShouldBeNil)
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = HDel(key, field)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
//
// func TestHExists(t *testing.T) {
//
// 	{
// 		Convey("TestHExists", t, func() {
//
// 			var err error
// 			key := "test_key_hash"
// 			field := "f"
// 			value := "abc"
//
// 			err = HSet(key, field, value)
// 			So(err, ShouldBeNil)
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = HExists(key, field)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
//
// func TestHGet(t *testing.T) {
//
// 	{
// 		Convey("TestHGet", t, func() {
//
// 			var err error
// 			key := "test_key_hash"
// 			field := "f"
// 			value := "abc"
//
// 			err = HSet(key, field, value)
// 			So(err, ShouldBeNil)
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				v, e := HGet(key, field)
// 				So(e, ShouldBeNil)
// 				So(v, ShouldEqual, value)
// 			})
// 		})
// 	}
// }
//
//
// func TestLLen(t *testing.T) {
//
// 	{
// 		Convey("TestLLen", t, func() {
//
// 			var err error
// 			key := "test_key_list"
// 			value := 0
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				v, e := LLen(key)
// 				So(e, ShouldBeNil)
// 				So(v, ShouldEqual, value)
// 			})
// 		})
// 	}
// }
//
//
// func TestLPush(t *testing.T) {
//
// 	{
// 		Convey("TestLPush", t, func() {
//
// 			var err error
// 			key := "test_key_list"
// 			value := "abc"
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = LPush(key, value)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
//
// func TestRPush(t *testing.T) {
//
// 	{
// 		Convey("TestRPush", t, func() {
//
// 			var err error
// 			key := "test_key_list"
// 			value := "abc"
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				err = RPush(key, value)
// 				So(err, ShouldBeNil)
// 			})
// 		})
// 	}
// }
//
//
// func TestBRPop(t *testing.T) {
//
// 	{
// 		Convey("TestBRPop", t, func() {
//
// 			var err error
// 			key := "test_key_list"
// 			value := "abc"
//
// 			err = LPush(key, value)
// 			So(err, ShouldBeNil)
//
// 			Convey("success", func() {
//
// 				// 清理测试数据
// 				defer func() {
// 					err = Del(key)
// 					So(err, ShouldBeNil)
// 				}()
//
// 				v, e := BRPop(key)
// 				So(e, ShouldBeNil)
// 				So(v, ShouldEqual, value)
// 			})
// 		})
// 	}
// }

package handler

import (
	// . "bou.ke/monkey"
	"fmt"
	. "github.com/agiledragon/gomonkey/v2"
	. "github.com/smartystreets/goconvey/convey"
	"test_cicd/app/api/model/entity"
	"test_cicd/app/api/store/mysql"
	"testing"
)

func TestHello(t *testing.T) {

	{
		Convey("TestHello", t, func() {

			// var userID = 1
			// 清理测试数据
			// defer func() {
			// 	err := mysql.DeleteTestUserByID(userID)
			// 	So(err, ShouldBeNil)
			// }()

			// 使用包 "bou.ke/monkey"  不支持arm
			// Convey("fail", func() {
			//
			// 	// 打桩，模拟返回
			// 	guard := Patch(mysql.HasTestUserByID, func(_ int) (has bool, m entity.TestUser, err error) {
			// 		err = fmt.Errorf("网络错误")
			// 		return false, m, err
			// 	})
			// 	defer guard.Unpatch()
			//
			// 	err := Hello()
			// 	So(err, ShouldNotBeNil)
			// })
			//
			// Convey("success", func() {
			//
			// 	// 打桩，模拟返回
			// 	guard := Patch(mysql.HasTestUserByID, func(_ int) (has bool, m entity.TestUser, err error) {
			// 		return false, m, nil
			// 	})
			// 	defer guard.Unpatch()
			//
			// 	err := Hello()
			// 	So(err, ShouldBeNil)
			// })

			// 使用包 "github.com/agiledragon/gomonkey"  支持arm
			Convey("fail", func() {

				// 打桩，模拟返回
				patch := ApplyFunc(mysql.HasTestUserByID, func(_ int) (has bool, m entity.TestUser, err error) {
					err = fmt.Errorf("网络错误")
					return false, m, err
				})
				defer patch.Reset()

				err := Hello()
				So(err, ShouldNotBeNil)
			})

			Convey("success", func() {

				// 打桩，模拟返回
				patch := ApplyFunc(mysql.HasTestUserByID, func(_ int) (has bool, m entity.TestUser, err error) {
					return false, m, nil
				})
				defer patch.Reset()

				err := Hello()
				So(err, ShouldBeNil)
			})
		})
	}
}

func TestHello2(t *testing.T) {

	{
		Convey("TestHello2", t, func() {

			Convey("success", func() {
				err := Hello2()
				So(err, ShouldBeNil)
			})
		})
	}
}

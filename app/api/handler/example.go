package handler

import (
	"fmt"
	"test_cicd/app/api/store/mysql"
)

// Hello Hello
func Hello() error {

	id := 1
	has, _, err := mysql.HasTestUserByID(id)
	if err != nil {
		return err
	}

	if has {
		fmt.Println("Hello")
	}

	return nil
}

// Hello2 Hello2
func Hello2() error {

	id := 1
	fmt.Println(id)

	return nil
}

// Hello3 Hello3
func Hello3() error {

	id := 1
	fmt.Println(id)

	return nil
}

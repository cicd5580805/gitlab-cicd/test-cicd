package ecode

// 负数的错误码直接弹框进行展示。如创建用户，用户参数缺失
// 正数错误码前端进行拦截处理，如果前端没有进行拦截那么不做任何提示。如创建用户信息，用户已经存在
// 业务错误码统一长度6位数，前3位是业务模块，后3位具体业务
// 详见：https://shimo.im/docs/G6cThrxT6JxJYGtx/read
const (
	OK                  = 0    // 请求成功。一般用于GET与POST请求
	Created             = -201 // 已创建。成功请求并创建了新的资源
	NoContent           = -204 // 无内容。服务器成功处理，但未返回内容。在未更新网页的情况下，可确保浏览器继续显示当前文档
	BadRequest          = -400 // 请求参数错误
	Unauthorized        = -401 // 请求要求用户的身份认证
	Forbidden           = -403 // 没有操作权限
	NotFound            = -404 // 服务器无法根据客户端的请求找到资源（网页）。通过此代码，网站设计人员可设置"您所请求的资源无法找到"的个性页面
	InternalServerError = -500 // 服务器内部错误，无法完成请求
)

// GetMsg 根据代码获取错误信息
func GetMsg(code int) string {
	msg, ok := MsgFlags[code]
	if ok {
		return msg
	}
	return MsgFlags[InternalServerError]
}

package config

import (
	"errors"
	"fmt"

	"github.com/spf13/viper"
	"pkg.deepin.com/service/lib/log"
	"pkg.deepin.com/service/lib/storage/db/v8"
	"pkg.deepin.com/service/lib/storage/rediser/v8"
)

// ServerConf Server配置
type ServerConf struct {
	Address string
}

// RPCServer RPC server配置结构体
type RPCServer struct {
	Host string
	Port int
}

var (
	rpcServerConf = &RPCServer{}
	serverConfig  = &ServerConf{}
)

// InitConfig InitConfig
func InitConfig() (err error) {
	viper.SetConfigFile("./config.toml")       // 指定配置文件路径
	viper.SetConfigName("config")              // 配置文件名称(无扩展名)
	viper.SetConfigType("toml")                // 如果配置文件的名称中没有扩展名，则需要配置此项
	viper.AddConfigPath("./conf/")             // 还可以在工作目录中查找配置
	viper.AddConfigPath("./../conf/")          // 多次调用以添加多个搜索路径（go test ./controller 时候使用）
	viper.AddConfigPath("./../../conf/")       // 多次调用以添加多个搜索路径（go test ./... 时候使用）
	viper.AddConfigPath("../../app/api/conf/") // 多次调用以添加多个搜索路径（go test ./... 时候使用）
	err = viper.ReadInConfig()                 // 查找并读取配置文件
	if err != nil {                            // 处理读取配置文件的错误
		return
	}

	// rpc 配置
	err = viper.UnmarshalKey("rpc.server", rpcServerConf)
	if rpcServerConf.Host == "" || rpcServerConf.Port == 0 {
		err = errors.New("rpc.server is empty")
		return
	}

	// server 配置
	err = viper.UnmarshalKey("server.http", serverConfig)
	if serverConfig.Address == "" {
		err = errors.New("server.http is empty")
		return
	}
	if serverConfig.Address == "" {
		err = errors.New("server.Address is empty")
		return
	}

	return nil
}

// Server Server配置
func Server() *ServerConf {
	return serverConfig
}

// RPCServerAddress RPCServer地址
func RPCServerAddress() string {
	return fmt.Sprintf("%s:%d", rpcServerConf.Host, rpcServerConf.Port)
}

// DB 数据库配置
func DB() (conf *db.Conf, err error) {
	conf = &db.Conf{}
	err = viper.UnmarshalKey("db", conf)
	return
}

// Redis Redis配置
func Redis() (conf *rediser.Conf, err error) {
	conf = &rediser.Conf{}
	err = viper.UnmarshalKey("redis", conf)
	return
}

// Log 日志配置
func Log() (conf *log.Options, err error) {
	conf = &log.Options{}
	err = viper.UnmarshalKey("log", conf)
	return
}

package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"test_cicd/app/api/utils/ecode"
)

// Response 响应结构体
type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// Resp HTTP层状态码统一为200,根据内层状态码code区分
func Resp(c *gin.Context, errCode int, msg string, data interface{}) {
	if msg == "" {
		msg = ecode.GetMsg(errCode)
	}
	respBody := Response{
		Code: errCode,
		Msg:  msg,
		Data: data,
	}
	c.JSON(http.StatusOK, respBody)
}

// OK 返回正确, data参数可选, body是否带有data字段由data参数决定
func OK(c *gin.Context, data interface{}) {
	Resp(c, ecode.OK, "", data)
}

// Fail 响应失败
func Fail(c *gin.Context, errCode int, msg string) {
	Resp(c, errCode, msg, nil)
}

// BadRequest 请求参数错误
func BadRequest(c *gin.Context, msg string) {
	Resp(c, ecode.BadRequest, msg, nil)
}

// InternalServerError 服务器内部错误
func InternalServerError(c *gin.Context, msg string) {
	Resp(c, ecode.InternalServerError, msg, nil)
}

// Forbidden 没有操作权限
func Forbidden(c *gin.Context, msg string) {
	Resp(c, ecode.Forbidden, msg, nil)
}

// NotFound 未找到资源
func NotFound(c *gin.Context, msg string) {
	Resp(c, ecode.NotFound, msg, nil)
}

// Unauthorized 未授权
func Unauthorized(c *gin.Context, msg string) {
	Resp(c, ecode.Unauthorized, msg, nil)
}

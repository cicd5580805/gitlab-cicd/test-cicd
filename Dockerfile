FROM hub.deepin.com/library/golang:bullseye AS builder
ARG MAIN_PATH
ARG APP_NAME
ARG ARCH
ARG GO_MOD
WORKDIR /src
ADD  . .
ENV CGO_ENABLED=0
RUN --mount=type="cache,target=/root/.cache/go-build" make build MAIN_PATH=${MAIN_PATH} APP_NAME=${APP_NAME} ARCH=${ARCH} GO_MOD=${GO_MOD}
RUN if [ ! -d /src/${MAIN_PATH}/data ]; then mkdir -p /src/${MAIN_PATH}/data; fi

FROM hub.deepin.com/public/alpine:3.16.0-udcp AS runner
WORKDIR /service
ARG PROJECT_NAME
ARG BRANCH
ARG COMMIT_ID
ARG APP_VERSION
ARG BUILD_DATE
ARG MAIN_PATH
ARG APP_NAME
ENV UDCP_APP_NAME="${PROJECT_NAME}/${APP_NAME}"
ENV UDCP_BRANCH="${BRANCH}"
ENV UDCP_COMMIT_ID="${COMMIT_ID}"
ENV UDCP_APP_VERSION="${APP_VERSION}"
ENV UDCP_BUILD_DATE="${BUILD_DATE}"
COPY --from=builder /src/${MAIN_PATH}/${APP_NAME} /service/api
COPY --from=builder /src/${MAIN_PATH}/conf /service/conf
COPY --from=builder /src/${MAIN_PATH}/data /service/data
CMD ["/service/api"]

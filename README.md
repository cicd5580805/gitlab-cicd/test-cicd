# test_cicd

test cicd

#### 触发编译docker镜像

- 为了解决部分同事没有CI权限的问题

调用以下命令可触发编译docker镜像

```shell
curl -X POST \
     -F token=glptt-ee780eb2905dd0e1e8df27580cfe44c15b35c98f \
     -F "ref=dev" \
     -F "variables[ARCH]=amd64" \
     -F "variables[RUN_MODE]=build_image" \
     -F "variables[VERSION]=V0.0.1-test" \
     -F "variables[NOTIFY]=enabled" \
     https://gitlabwh.uniontech.com/api/v4/projects/36425/trigger/pipeline
```
- 修改参数值可以达到编译不同分支，不同标签镜像
```text
相关变量说明
token: Settings -> CI/CD -> Pipeline triggers 配置的token，项目内已配置好；
ref: 分支/标签名称；
variables: pipeline相关传入变量，目前编译docker镜像需有4个变量，ARCH表示架构，取值amd64与arm64；RUN_MODE表示运行模式，取值build_image（表示仅编译docker镜像并推送至hub）；VERSION表示版本，即镜像标签，按需要填写，如填写V2.0.0.1，对于api-auth项目，则完整的镜像名称为hub.deepin.com/wuhan_udcp/api-auth/api:V2.0.0.1；NOTIFY表示是否启用企业微信机器人通知，enabled为通知，disabled为不通知；
36425: project id，可在项目仓库首页查看
```
图片示例：
![image](docs/static/run-pipeline-1.png)


也可使用以下命令（调研hook服务接口，注意按照后文修改对应参数值）
```shell
curl "http://10.0.35.191/pipeline?type=build_image&token=glptt-ee780eb2905dd0e1e8df27580cfe44c15b35c98f&project_url=$CI_PROJECT_URL&project_id=$CI_PROJECT_ID&ref=$CI_COMMIT_REF_NAME&arch=$ARCH&version=$VERSION&run_mode=build_image&notify=enabled"
```
链接地址示例：http://10.0.35.191/pipeline?type=build_image&token=glptt-ee780eb2905dd0e1e8df27580cfe44c15b35c98f&project_url=$CI_PROJECT_URL&project_id=$CI_PROJECT_ID&ref=$CI_COMMIT_REF_NAME&arch=$ARCH&version=$VERSION&run_mode=build_image&notify=enabled
- 修改上述链接地址内参数值可以达到编译不同分支，不同标签镜像
```text
相关变量说明
type: pipeline类型，取值build_image（表示创建编译docker镜像pipeline）；
token: Settings -> CI/CD -> Pipeline triggers 配置的token，项目内已配置好；
project_url: 项目地址，如，https://gitlabwh.uniontech.com/ut002239/test_cicd；
project_id: 项目id，可在项目仓库首页查看，如，36425；
ref: 分支/标签名称；
arch: 架构，取值amd64与arm64；
version: 版本，即镜像标签，按需要填写，如填写V2.0.0.1，对于api-auth项目，则完整的镜像名称为hub.deepin.com/wuhan_udcp/api-auth/api:V2.0.0.1；
run_mode: 运行模式，取值build_image（表示仅编译docker镜像并推送至hub）；
notify: 是否启用企业微信机器人通知，enabled为通知，disabled为不通知；
```

```text
原理说明：
10.0.35.191服务器上部署有nginx与hook服务，请求"http://10.0.35.191/pipeline"由nginx反向代理至hook服务（代码见udcp_fabric/tools/hook/）；
hook服务收到请求后，调用gitlab API，触发pipeline，相当于有ci权限时新建一个pipeline；
前提：Settings -> CI/CD -> Pipeline triggers 内需要配置token；
```


# 目录结构
```
test_cicd                           #项目根路径
│                          
│   └── app #示例项目
│       └── api                     #对外暴露接口的service
│           ├── conf                #配置文件
│           ├── consumer            #负责做rpc调用
│           │   └── user
│           ├── controller          #接收req请求,并且做参数处理及校验
│           ├── handler             #业务逻辑
│           ├── logs                #日志
│           ├── middleware          #中间件
│           ├── model               #api model针对外部的struct
│           │   ├── ecode
│           │   └── user
│           ├── store               #存储(db、redis等)
│           │   └── user
│           ├── utils               #工具               
│           │    ├── httptest
│           │    └── response       #统一响应格式 
│           └── validate            #参数校验工具
├── pkg                             #公共工具
│   └── proto                       #proto
│       └── rmsRpc                  #rpc方法protobuf定义
│   
├── vendor                          #go mod require
├── Dockerfile                      #Dockerfile镜像编译打包
└── Makefile                        #Makefile编译

```

# 服务基本构成

一个服务应该有API、RPC、CRON三部分组成。
- API：对集群外的应用提供服务 HTTP 服务
- CRON：定时任务服务
- RPC：对集群内的应用提供服务 RPC 服务

## API 服务

对集群外的应用提供服务HTTP服务。
- 原则上API所有依赖都限于api文件夹，如果某种服务A需要同时对内和对外提供，那么建议服务A由RPC提供，API调用RPC来实现服务A。
- req请求处理顺序是 middleware->controller->handler->store。

#### 主进程启动

```shell script
    # export GIN_MODE=true # 调试模式
    go run main.go
```

#### 目录结构
```
api
├── conf        #配置文件
├── consumer    #负责做rpc调用
├── controller  #接收req请求,并且做参数处理及校验
├── handler     #业务逻辑
├── middleware  #中间件
├── model       #数据模型
├── store       #存储模型
└── utils       #基础类库
```
- conf
```
配置文件文件夹，只包含config.toml一个文件。
config.toml是本地联调配置，跟随代理提交到代码仓库中
测试环境、预发布环境、生产环境使用容器文件挂载的方式进行覆盖
```
- consumer
```
rpc客户端，负责处理rpc调用
```
- controller
```
接收req请求,并且做参数处理及校验,原则上不处理复杂业务逻辑
```
- handler
```
调用store和consumer和model处理业务逻辑，原则上不允许直接连接存储，可以调用store来连接存储
```
- middleware
```
中间件，处理鉴权等公用逻辑
```
- model
```
http请求数据模型、http响应数据模型、表格数据模型等各种数据模型
```
- store
```
db、redis等存储相关，原则上不处理具体业务逻辑，只操作存储
```
- utils
```
API服务基础类库，如httptest类库、json响应类型。如果rpc或者其他项目也会使用可以考虑放到pkg目录中
```

## CRON 定时任务

定时任务服务

- CRON可以直接调用RPC或者API的model和handler来处理业务逻辑
- 原则上不写model或者handler

#### 主进程启动

```shell script
    go run main.go
```

#### 目录结构

```
./cron/
├── logs #日志
└── user #业务定时任务
```

#### 调用RPC或者API的model或者handler来处理业务逻辑

- 直接调用API model或者handler需要指定配置文件路径为api/conf/config.toml。详见cron/utils/config/conf.go

## RPC 服务（todo）

对集群内的应用提供服务 RPC 服务
- 原则上RPC所有依赖都限于rpc文件夹，不允许调用api文件夹中的任何代码
- 如果上K8S，那么gRPC addr直接填写服务名称即可实现服务注册和发现

#### 主进程启动

```shell script
    go run main.go
```

#### 目录结构
```
./rpc/
├── conf        #配置文件
├── controller  #接收请求,并且做参数处理及校验
├── handler     #业务逻辑
├── logs
├── model       #数据模型       
├── store       #存储模型
└── utils       #基础类库
```

# 单元测试

用API服务进行举例说明，更多单元测试和打桩用例可以查看：

```shell script
https://gitlabwh.uniontech.com/wuhan/golang/testdemo
```

## 单元测试执行

**注意**
> 单元测试包 "bou.ke/monkey"，需注意该包不支持arm架构
> 单元测试包 "github.com/agiledragon/gomonkey"，支持arm

- 执行所有单元测试

```shell script
# 进入项目根目录执行：
    go test ./... -count=1 -v
```

- controller 单元测试执行方法

```shell script
# 进入项目根目录执行：
    go test ./controller -count=1 -v
# 执行指定单元测试方法：
    go test ./controller -count=1 -v -run=TestInfo
```

- handler 单元测试执行方法

```shell script
# 进入项目根目录执行：
    go test ./handler -count=1 -v
# 执行指定单元测试方法：
  go test ./handler -count=1 -v -run=TestInfo
```

- store 单元测试执行方法

```shell script
# 进入项目根目录执行：
    go test ./store/... -count=1 -v
# 执行指定单元测试方法：
    go test ./store/... -count=1 -v -run=TestGetUserByName
```

- consumer 单元测试执行方法

```shell script
# 进入项目根目录执行：
    go test ./consumer/... -count=1 -v
# 执行制定单元测试方法：
    go test ./consumer/... -count=1 -v -run=TestGetUserByName
```

# 前后端接口规范

前后端接口规范详情见：https://shimo.im/docs/G6cThrxT6JxJYGtx/read
参考文件: /api/utils/response/response.go

## 响应结构体：

```shell script
type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}
```

## 错误码说明：

- 负数的错误码直接弹框进行展示。如创建用户，用户参数缺失
- 正数错误码前端进行拦截处理，如果前端没有进行拦截那么不做任何提示。如创建用户信息，用户已经存在
- 业务错误码统一长度6位数，前3位是业务模块，后3位具体业务

# HTTP框架

Gin是用Go（Golang）编写的Web框架。它具有类似于martini的API，其性能比httprouter快40倍

详细说明：https://github.com/gin-gonic/gin

# 日志库

log库对zap库 (github.com/uber-go/zap) 进行了简要的封装，支持输出json格式的日志，日志分割，实现了go-micro/logger接口。

详细说明：https://gitlabwh.uniontech.com/wuhan/service/lib/log

# DB库

db基于gorm (github.com/jinzhu/gorm) 封装，支持一主多从、JSON格式日志，慢日志提示。

详细说明：https://gitlabwh.uniontech.com/wuhan/service/lib/storage/db

# Redis库

rediser基于go-redis (github.com/go-redis/redis) 封装，支持一主多从、JSON格式日志，慢日志提示。

详细说明：https://gitlabwh.uniontech.com/wuhan/service/lib/storage/rediser

# 配置文件库

Viper是适用于Go应用程序（包括12因子应用程序）的完整配置解决方案。

详细说明：https://github.com/spf13/viper

# 定时任务库

开源定时任务库

详细说明：https:github.com/robfig/cron/v3

# 示例运行依赖DB和redis

- creat log path

```shell script
sudo mkdir /home/www/logs/example
sudo chmod 733 /home/www/logs/example
```

- db

```
sudo docker run -p 3306:3306 --name mysql \
	-v /usr/local/docker/mysql/conf:/etc/mysql \
	-v /usr/local/docker/mysql/logs:/var/log/mysql \
	-v /usr/local/docker/mysql/data:/var/lib/mysql \
	-e MYSQL_ROOT_PASSWORD=PP94pMS8nvP^y1FW@bYcmxpTThMjrtHU \
	-d mysql:5.7
```

- redis

```
docker run -p 6379:6379 --name redis \
    -v /usr/local/docker/redis/redis.conf:/etc/redis/redis.conf \
    -v /usr/local/docker/redis/data:/data \
    -d redis redis-server \
    /etc/redis/redis.conf \
    --appendonly yes
```


### 不使用vendor时，公司私有代码仓库无法拉取
#### 方式1：已验证不可行
1.公司私有代码仓库gitlabwh.uniontech.com内-->个人设置（user settings）--> Access Tokens新建一个token，权限只有read_api, read_user, read_repository
2.git配置添加access token
```shell
git config http.extraheader "PRIVATE-TOKEN: YOUR_PRIVATE_TOKEN"
```
3.使用SSH而不是HTTPS
```shell
git config url."git@gitlab.yoursite.com:".insteadof "https://gitlab.yoursite.com/"
```
- 参考文档：
> https://www.jianshu.com/p/ca4404512cf3
> http://haodro.com/archives/334103
> https://blog.csdn.net/ijijni/article/details/118891606

#### 方式2：已验证可行
- 此方式将ssh公私钥保存在代码仓库，不安全
1.生成ssh公私钥
```shell
ssh-keygen -t rsa -b 2048 -C "udcp_ci_reboot"
```
2.将ssh公钥添加到公司私有代码仓库gitlabwh.uniontech.com内-->个人设置（user settings）--> SSH Keys
```shell
cat ~/.ssh/id_rsa.pub
```
3.将ssh目录放入代码仓库
4.ci内拷贝ssh目录至家目录，修改ssh私钥权限，修改ssh配置



package rediser

// Group Redis组
type Group struct {
	*Conn
	isCluster bool
	slaves []*Conn
	policy GroupPolicy
}

// NewGroup 新建redis组
func NewGroup(groupConf *GroupConf, policies ...GroupPolicy) (group *Group, err error) {
	var conn *Conn
	group = &Group{}
	group.isCluster = false
	if len(policies) > 0 {
		group.policy = policies[0]
	} else {
		group.policy = RandomPolicy()
	}

	//master和slave配置
	masterConf := groupConf.Master
	slaveConf := groupConf.Slave

	if len(masterConf.DSN) > 1 {
		group.isCluster = true
	}
	// master连接
	conn, err = NewConn(&ConnConf{
		DSN:           masterConf.DSN,
		Password:      masterConf.Password,
		DB:            masterConf.DB,
		DialTimeout:   masterConf.DialTimeout,
		ReadTimeout:   masterConf.ReadTimeout,
		WriteTimeout:  masterConf.WriteTimeout,
		PoolSize:      masterConf.PoolSize,
		MinIdleConns:  masterConf.MinIdleConns,
		PoolTimeout:   masterConf.PoolTimeout,
		IdleTimeout:   masterConf.IdleTimeout,
		LogMode:       masterConf.LogMode,
		SlowThreshold: masterConf.SlowThreshold,
		SlaveFlag:     false,
	})
	if err != nil {
		return
	}
	group.Conn = conn
	// 如果是集群，则不配置slave
	if group.isCluster {
		return
	}

	// slave连接池
	if slaveConf.DSN != nil {
		for i := 0; i < len(slaveConf.DSN); i++ {
			conn, err = NewConn(&ConnConf{
				DSN:           slaveConf.DSN[i:i+1],
				Password:      slaveConf.Password,
				DB:            slaveConf.DB,
				DialTimeout:   slaveConf.DialTimeout,
				ReadTimeout:   slaveConf.ReadTimeout,
				WriteTimeout:  slaveConf.WriteTimeout,
				PoolSize:      slaveConf.PoolSize,
				MinIdleConns:  slaveConf.MinIdleConns,
				PoolTimeout:   slaveConf.PoolTimeout,
				IdleTimeout:   slaveConf.IdleTimeout,
				LogMode:       slaveConf.LogMode,
				SlowThreshold: slaveConf.SlowThreshold,
				SlaveFlag:     true,
			})
			if err != nil {
				return
			}
			group.slaves = append(group.slaves, conn)
		}
	}
	return
}

// SetPolicy 设置获取slave的策略
func (group *Group) SetPolicy(policy GroupPolicy) {
	group.policy = policy
	return
}

// Master 获取master连接
func (group *Group) Master() (conn *Conn) {
	return group.Conn
}

// Slave 根据策略获取某slave连接
func (group *Group) Slave() (conn *Conn) {
	if group.isCluster {
		return group.Conn
	}

	switch len(group.slaves) {
	case 0:
		return group.Conn
	case 1:
		return group.slaves[0]
	}
	return group.policy.Slave(group)
}

// Slaves 获取所有slave连接
func (group *Group) Slaves() []*Conn {
	if group.isCluster {
		return []*Conn{group.Conn}
	}

	return group.slaves
}

// Ping 验证连接是否正常
func (group *Group) Ping() (err error) {
	_, err = group.Cmdable.Ping().Result()
	if err != nil {
		return
	}
	for _, v := range group.slaves {
		_, err = v.Ping().Result()
		if err != nil {
			return
		}
	}
	return nil
}

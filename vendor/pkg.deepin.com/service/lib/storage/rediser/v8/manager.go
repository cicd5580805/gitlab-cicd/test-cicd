package rediser

var defaultManager *Manager

// Manager 单例redis管理器
type Manager struct {
	groupMap map[string]*Group
}

// NewManager 新建一个redis管理器
func NewManager(config *Conf) (mgr *Manager, err error) {
	mgr = &Manager{
		groupMap: make(map[string]*Group),
	}

	if config == nil {
		return
	}

	// 按Name索引每一个Redis Group
	for k, v := range *config {
		var group *Group
		if group, err = NewGroup(&v); err != nil {
			return
		}
		mgr.groupMap[k] = group
	}

	return
}

// InitManager 初始化defaultManager
func InitManager(config *Conf) {
	mgr, err := NewManager(config)
	if err != nil {
		panic(err)
	}
	defaultManager = mgr
}

// Group 根据name获取Redis Group
func (mgr *Manager) Group(name string) (group *Group) {
	return mgr.groupMap[name]
}

// Master 获取master连接
func (mgr *Manager) Master(name string) *Conn {
	if mgr.Group(name) == nil {
		return &Conn{}
	}

	return mgr.Group(name).Master()
}

// Slave 获取slave连接
func (mgr *Manager) Slave(name string) *Conn {
	if mgr.Group(name) == nil {
		return &Conn{}
	}
	return mgr.Group(name).Slave()
}

// DefaultManager 获取defaultMgr实例
func DefaultManager() *Manager {
	return defaultManager
}

// Master 获取master连接
func Master(name string) *Conn {
	if defaultManager.Group(name) == nil {
		return &Conn{}
	}

	return defaultManager.Group(name).Master()
}

// Slave 获取slave连接
func Slave(name string) *Conn {
	if defaultManager.Group(name) == nil {
		return &Conn{}
	}
	return defaultManager.Group(name).Slave()
}

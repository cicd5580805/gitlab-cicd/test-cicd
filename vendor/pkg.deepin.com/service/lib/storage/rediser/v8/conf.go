package rediser

// Conf Manager配置
type Conf map[string]GroupConf

// GroupConf Redis主从配置
type GroupConf struct {
	Master MasterConf
	Slave  SlaveConf
}

// MasterConf Master配置
type MasterConf struct {
	DSN           []string // 地址
	Password      string // 密码
	DB            int    // 设置连接到server后选择的DB，默认值为0
	DialTimeout   int    // 连接超时时间,单位：ms，默认值为5s
	ReadTimeout   int    // 读超时时间,单位：ms，默认值为3s
	WriteTimeout  int    // 写超时时间,单位：ms，默认值为3s
	PoolSize      int    // 连接池大小
	MinIdleConns  int    // 最小空闲连接数
	PoolTimeout   int    // 连接池获取的超时时间，单位：ms，默认值ReadTimeout+1s
	IdleTimeout   int    // 最大空闲时长, 超过后关闭连接，单位：ms，默认值5m
	LogMode       bool   // 设置是否显示debug日志
	SlowThreshold int    // 设置慢日志阈值，单位：ms
}

// SlaveConf Slave配置
type SlaveConf struct {
	DSN           []string
	Password      string
	DB            int
	DialTimeout   int
	ReadTimeout   int
	WriteTimeout  int
	PoolSize      int
	MinIdleConns  int
	PoolTimeout   int
	IdleTimeout   int
	LogMode       bool
	SlowThreshold int
}

// ConnConf 连接配置
type ConnConf struct {
	DSN           []string
	Password      string
	DB            int
	DialTimeout   int
	ReadTimeout   int
	WriteTimeout  int
	PoolSize      int
	MinIdleConns  int
	PoolTimeout   int
	IdleTimeout   int
	SlaveFlag     bool
	LogMode       bool
	SlowThreshold int
}

// ClusterConf 集群配置
type ClusterConf struct {
	DSN           []string
	Password      string
	DialTimeout   int
	ReadTimeout   int
	WriteTimeout  int
	PoolSize      int
	MinIdleConns  int
	PoolTimeout   int
	IdleTimeout   int
	LogMode       bool
	SlowThreshold int
}

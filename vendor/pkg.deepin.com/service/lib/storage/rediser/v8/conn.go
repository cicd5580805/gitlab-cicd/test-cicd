package rediser

import (
	"fmt"
	"strings"
	"github.com/go-redis/redis"
	"pkg.deepin.com/service/lib/log"
)

var defaultConn *Conn

// Conn 单个redis连接
type Conn struct {
	redis.Cmdable
	conf *ConnConf
}

// NewConn 新建redis连接
func NewConn(conf *ConnConf) (conn *Conn, err error) {
	var client redis.Cmdable
	// 如果是集群模式
	if len(conf.DSN) > 1 {
		client, err = NewClusterClient(conf)
	} else{
		client, err = NewSingleClient(conf)
	}
	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}

	conn = &Conn{
		client,
		conf,
	}

	return
}

// InitConn 初始化一个redis连接
func InitConn(conf *ConnConf) {
	var err error
	defaultConn, err = NewConn(conf)
	if err != nil {
		panic(err)
	}
	log.Info("init redis connection success", "dsn", defaultConn.conf.DSN, "db", defaultConn.conf.DB)
}

// Default 获取defaultConn实例
func Default() *Conn {
	return defaultConn
}

// formatCmds 格式化redis命令用于展示
func formatCmds(cmders []redis.Cmder) string {
	var cmdList = make([]string, 0)

	// 对于每一个redis请求
	for _, cmd := range cmders {
		// 拼接命令参数
		var args = make([]string, 0)
		for _, arg := range cmd.Args() {
			args = append(args, fmt.Sprint(arg))
		}
		argsStr := strings.Join(args, " ")
		if err := cmd.Err(); err != nil {
			argsStr = fmt.Sprintf("%s(%s)", argsStr, err.Error())
		}
		cmdList = append(cmdList, argsStr)
	}
	return strings.Join(cmdList, "\n")
}
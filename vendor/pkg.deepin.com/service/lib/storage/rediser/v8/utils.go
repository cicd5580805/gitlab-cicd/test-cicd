package rediser

import (
	"fmt"
	"regexp"
	"runtime"
)

// Utils ...
type Utils struct{}

// FileWithLineNum 获取db_caller文件及代码位置
func (utils *Utils) FileWithLineNum() string {
	var goSrcRegexp = regexp.MustCompile(`github.com/go-redis/redis(@.*)?/.*.go`)
	var goTestRegexp = regexp.MustCompile(`github.com/go-redis/redis(@.*)?/.*test.go`)
	var connSrcRegexp = regexp.MustCompile(`conn.go`) // TOFIX: 'pkg.deepin.com/service/lib/rediser/conn.go'

	for i := 2; i < 15; i++ {
		_, file, line, ok := runtime.Caller(i)
		if ok && ((!goSrcRegexp.MatchString(file) || goTestRegexp.MatchString(file)) && !connSrcRegexp.MatchString(file)) {
			return fmt.Sprintf("%v:%v", file, line)
		}
	}
	return ""
}

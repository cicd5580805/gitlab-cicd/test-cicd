package rediser

import (
	"time"

	"github.com/go-redis/redis"
	"pkg.deepin.com/service/lib/log"
)

var defaultClusterClient *ClusterClient

// ClusterClient 集群连接客户端
type ClusterClient struct {
	*redis.ClusterClient
	conf *ConnConf
}

// NewClusterClient 新建集群连接客户端
func NewClusterClient(conf *ConnConf) (clusterClient *ClusterClient, err error) {
	client := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs:        conf.DSN,
		Password:     conf.Password,                                       //传递密码
		DialTimeout:  time.Duration(conf.DialTimeout) * time.Millisecond,  //闲置重新建立连接数时间
		ReadTimeout:  time.Duration(conf.ReadTimeout) * time.Millisecond,  //设置读超时时间
		WriteTimeout: time.Duration(conf.WriteTimeout) * time.Millisecond, //设置写超时时间
		PoolSize:     conf.PoolSize,
		MinIdleConns: conf.MinIdleConns,
		PoolTimeout:  time.Duration(conf.PoolTimeout) * time.Millisecond,
		IdleTimeout:  time.Duration(conf.IdleTimeout) * time.Millisecond,
	})

	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}

	clusterClient = &ClusterClient{
		client,
		conf,
	}

	client.WrapProcess(replaceClusterProcess(clusterClient))
	client.WrapProcessPipeline(replaceClusterProcessPipeline(clusterClient))

	return
}

// InitClusterClient 初始化一个redis集群客户端
func InitClusterClient(conf *ConnConf) {
	var err error
	defaultClusterClient, err = NewClusterClient(conf)
	if err != nil {
		panic(err)
	}
	log.Info("init redis cluster connection success", "dsn", defaultClusterClient.conf.DSN)
}

// DefaultCluster 获取defaultClusterClient实例
func DefaultClusterClient() *ClusterClient {
	return defaultClusterClient
}

func replaceClusterProcess(conn *ClusterClient) func(old func(cmd redis.Cmder) error) func(cmd redis.Cmder) error {
	return func(old func(cmd redis.Cmder) error) func(cmd redis.Cmder) error {
		return func(cmd redis.Cmder) (err error) {
			s := formatCmds([]redis.Cmder{cmd})

			beg := time.Now()
			err = old(cmd)
			cost := time.Since(beg)
			if err == redis.Nil {
				//log.Warn("warn redis log", "redis_caller", new(Utils).FileWithLineNum(), "err", err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			} else if err != nil {
				log.Error("error redis log", "redis_caller", new(Utils).FileWithLineNum(), "err", err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			// 慢日志
			if conn.conf.SlowThreshold > 0 && cost > time.Duration(conn.conf.SlowThreshold)*time.Millisecond {
				log.Warn("slow redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			// debug日志
			if conn.conf.LogMode {
				log.Debug("debug redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
			}

			return
		}
	}
}

func replaceClusterProcessPipeline(conn *ClusterClient) func(old func([]redis.Cmder) error) func([]redis.Cmder) error {
	return func(old func([]redis.Cmder) error) func([]redis.Cmder) error {
		return func(cmders []redis.Cmder) (err error) {
			s := formatCmds(cmders)

			beg := time.Now()
			err = old(cmders)
			cost := time.Since(beg)
			if err == redis.Nil {
				log.Warn("warn redis log", "redis_caller", new(Utils).FileWithLineNum(), "err", err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			} else if err != nil {
				log.Error("error redis log", "redis_caller", new(Utils).FileWithLineNum(), err, err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			// 慢日志
			if conn.conf.SlowThreshold > 0 && cost > time.Duration(conn.conf.SlowThreshold)*time.Millisecond {
				log.Error("slow redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			// debug日志
			if conn.conf.LogMode {
				log.Debug("debug redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
			}

			return
		}
	}
}

package rediser

import (
	"errors"

	"github.com/go-redis/redis"
)

const (
	Nil = redis.Nil
)

// 错误码
var (
	ERR_CACHE_CONN_NIL = errors.New("Redis connection is nil")
)

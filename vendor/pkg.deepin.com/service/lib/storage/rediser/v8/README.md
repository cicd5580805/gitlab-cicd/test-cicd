rediser基于go-redis ([github.com/go-redis/redis]()) 封装，支持一主多从、集群、JSON格式日志，慢日志提示。

### 安装

------

```
go get pkg.deepin.com/service/lib/storage/rediser/v8
```

### 快速开始

------

#### 单数据库

```
package main

import (
	"pkg.deepin.com/service/lib/storage/rediser/v8"
	"pkg.deepin.com/service/utils/config"
)

func main() {
	// 加载配置文件
	type MyfConfig struct {
		Redis *rediser.ConnConf // Redis配置
	}
	var cfg MyfConfig
	err := config.Load("redis.toml", &cfg)
	if nil != err {
		panic(err)
	}

	// 初始化默认的redis链接
	rediser.InitConn(cfg.Redis)

	// set key value
	err = rediser.Default().Set("key", "value", 0).Err()
	if err != nil {
		panic(err)
	}

	// get key
	val, err := rediser.Default().Get("key").Result()
	if err == rediser.Nil {
		fmt.Println("key does not exist")
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key", val)
	}

	// del key
	_, err = rediser.Default().Del("key").Result()
	if err != nil {
		panic(err)
	}
}
```

###### 单数据库配置文件格式

```
[Redis]
	dsn = ["10.0.10.27:6333"]    # 地址
	password = "123456"          # 密码
	db = 0                       # 设置连接到server后选择的DB，默认值为0
	logMode = true               # 设置是否开启debug日志，默认值为false
	slowThreshold = 5            # 设置慢日志阈值，单位ms，默认值为0（不开启慢日志）
	dialTimeout = 5000           # 连接超时时间，单位：ms，默认值为5000ms
	readTimeout = 3000           # 读超时时间，单位：ms，默认值为3000ms
	writeTimeout = 3000          # 写超时时间，单位：ms，默认值等于ReadTimeout
	poolSize = 80                # 连接池大小，默认值为cpu核数*10
	minIdleConns = 10            # 最小空闲连接数，默认值为0
	poolTimeout = 6000           # 连接池获取的超时时间，单位：ms，默认值ReadTimeout+1000ms
	idleTimeout = 300000         # 空闲连接的超时时间，默认值为5*60*1000ms
```

#### 一主多从

```
package main

import (
	"pkg.deepin.com/service/lib/storage/rediser/v8"
	"pkg.deepin.com/service/utils/config"
)

func main() {
	// 加载配置文件
	type MyfConfig struct {
		Redis *rediser.Conf // Redis Manager配置
	}
	var cfg MyfConfig
	err := config.Load("redis.toml", &cfg)
	if nil != err {
		panic(err)
	}
	
	// 初始化默认的Redis Manager
	rediser.InitManager(cfg.Redis)
	
	// 从Master写
	err = rediser.Master("test").Set("key", "value", 0).Err()
	if err != nil {
		panic(err)
	}

	// 从Slave读
	val, err := rediser.Slave("test").Get("key").Result()
	if err == rediser.Nil {
		fmt.Println("key does not exist")
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key", val)
	}

	// 从Master删除
	_, err = rediser.Master("test").Del("key").Result()
	if err != nil {
		panic(err)
	}
	
}

```

###### 一主多从配置文件格式

```
[Redis]
	[Redis.test]                   # Redis组名称:test
		[Redis.test.Master]        # Master配置
			dsn = ["10.0.10.27:6333"]
			password = "123456"
			slowThreshold = 5
		[Redis.test.Slave]         # Slave配置
			dsn = ["10.0.10.27:6334","10.0.10.27:6335"]
			password = "123456"
			slowThreshold = 5
```

#### 集群模式

```
package main

import (
	"fmt"
	"pkg.deepin.com/service/lib/storage/rediser/v8"

	"pkg.deepin.com/service/utils/config"
)

func main() {
	//加载配置文件
	type MyfConfig struct {
		RedisCluster *rediser.ClusterConf // Redis配置
	}
	var cfg MyfConfig
	err := config.Load("redis.toml", &cfg)
	if nil != err {
		panic(err)
	}

	// 初始化默认的redis链接
	rediser.InitClusterClient(cfg.RedisCluster)

	// set key value
	err = rediser.DefaultClusterClient().Set("key", "value", 0).Err()
	if err != nil {
		panic(err)
	}

	// get key
	val, err := rediser.DefaultClusterClient().Get("key").Result()
	if err == rediser.Nil {
		fmt.Println("key does not exist")
	} else if err != nil {
		panic(err)
	} else {
		fmt.Println("key", val)
	}

	// del key
	_, err = rediser.DefaultClusterClient().Del("key").Result()
	if err != nil {
		panic(err)
	}
}
```

###### 集群模式配置文件

```
[RedisCluster]
	dsn = ["10.20.12.248:7001", "10.20.12.248:7002", "10.20.12.248:7003", "10.20.12.248:7004", "10.20.12.248:7005", "10.20.12.248:7006"]
	password = "deepin!@#"
	slowThreshold = 5
	dialTimeout = 5000 
	readTimeout = 3000
	writeTimeout = 3000
	poolSize = 80 
	minIdleConns = 10 
	poolTimeout = 6000 
	idleTimeout = 300000
	logMode = true
```



#### 创建Conn

结构体参数创建单个连接。

```
NewConn(connConf *ConnConf)(conn *Conn, err error)
```

#### 创建Group

用于一主多从。

```
NewGroup(groupConf *GroupConf, policies ...GroupPolicy) (group *Group, err error)
```

其中policies为选择slave库的策略，目前主要实现了以下三种策略：

- RandomPolicy()： 随机选择Slave，默认策略。
- RoundRobinPolicy()：轮询选择Slave。
- WeightRandomPolicy(weights []int)：根据权重选择Slave，weights的长度依据Slave的长度而定。

此外，可参考GroupPolicy接口进行自定义策略。

```
group, _ := rediser.NewGroup(groupConf, rediser.RandomPolicy())
```

#### 创建Manager 

用于多个Group，通过名称获取Group。

```
NewManager(config *Conf) (mgr *Manager, err error)
```

```
mgr, err := rediser.NewManager(config)          // 创建Manager 
group, err := mgr.Group(groupName)              // 获取Group
```

#### 创建ClusterClient

用于集群模式。

```
NewClusterClient(conf *ClusterConf) (clusterClient *ClusterClient, err error)
```

#### 日志说明

rediser的日志等级分为debug、warn、error级别，慢日志的级别为warn，日志示例如下：

```
// error
{
	"level":"error",
	"time":"2020-07-17 17:02:06",
	"caller":"rediser/conn.go:118",
	"msg":"error redis log",
	"redis_caller":"/home/liaoshiwei/Desktop/缓存/封装/test/main.go:37",
	"err":"redis: nil",
	"addr":"10.0.10.27:6333",
	"cmd":"get test",
	"cost":"15.679969ms"
}
```

- level：日志等级。
- time：日志打印时间。
- caller：调用log库的代码所在处。
- redis_caller：调用rediser库的代码所在处。
- msg：日志描述。
- cmd：执行的命令。
- cost：执行redis命令的耗时。
- addr：redis服务器地址。
- err：具体错误信息。

ps：大家有其它需求或发现bug可以提issue哈，谢谢。

package rediser

import (
"time"

"github.com/go-redis/redis"
"pkg.deepin.com/service/lib/log"
)

var defaultSingleClient *SingleClient

// SingleClient 单个redis连接
type SingleClient struct {
	*redis.Client
	conf *ConnConf
}

// NewSingleClient 新建redis连接
func NewSingleClient(conf *ConnConf) (c *SingleClient, err error) {
	client := redis.NewClient(&redis.Options{
		Addr:         conf.DSN[0],
		Password:     conf.Password, //传递密码
		DB:           conf.DB,
		DialTimeout:  time.Duration(conf.DialTimeout) * time.Millisecond,  //闲置重新建立连接数时间
		ReadTimeout:  time.Duration(conf.ReadTimeout) * time.Millisecond,  //设置读超时时间
		WriteTimeout: time.Duration(conf.WriteTimeout) * time.Millisecond, //设置写超时时间
		PoolSize:     conf.PoolSize,
		MinIdleConns: conf.MinIdleConns,
		PoolTimeout:  time.Duration(conf.PoolTimeout) * time.Millisecond,
		IdleTimeout:  time.Duration(conf.IdleTimeout) * time.Millisecond,
	})

	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}

	c = &SingleClient{
		client,
		conf,
	}

	c.WrapProcess(replaceProcess(c))
	c.WrapProcessPipeline(replaceProcessPipeline(c))

	return
}

// InitSingleConn 初始化一个redis连接
func InitSingleConn(conf *ConnConf) {
	var err error
	defaultSingleClient, err = NewSingleClient(conf)
	if err != nil {
		panic(err)
	}
	log.Info("init redis connection success", "dsn", defaultConn.conf.DSN, "db", defaultConn.conf.DB)
}

// Default 获取defaultConn实例
func DefaultSingleClient() *SingleClient {
	return defaultSingleClient
}


func replaceProcess(conn *SingleClient) func(old func(cmd redis.Cmder) error) func(cmd redis.Cmder) error {
	return func(old func(cmd redis.Cmder) error) func(cmd redis.Cmder) error {
		return func(cmd redis.Cmder) (err error) {
			s := formatCmds([]redis.Cmder{cmd})

			beg := time.Now()
			err = old(cmd)
			cost := time.Since(beg)
			if err == redis.Nil {
				//log.Warn("warn redis log", "redis_caller", new(Utils).FileWithLineNum(), "err", err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			} else if err != nil {
				log.Error("error redis log", "redis_caller", new(Utils).FileWithLineNum(), "err", err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			// 慢日志
			if conn.conf.SlowThreshold > 0 && cost > time.Duration(conn.conf.SlowThreshold)*time.Millisecond {
				log.Warn("slow redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			if conn.conf.LogMode {
				log.Debug("debug redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
			}

			return
		}
	}
}

func replaceProcessPipeline(conn *SingleClient) func(old func([]redis.Cmder) error) func([]redis.Cmder) error {
	return func(old func([]redis.Cmder) error) func([]redis.Cmder) error {
		return func(cmders []redis.Cmder) (err error) {
			s := formatCmds(cmders)

			beg := time.Now()
			err = old(cmders)
			cost := time.Since(beg)
			if err == redis.Nil {
				log.Warn("warn redis log", "redis_caller", new(Utils).FileWithLineNum(), "err", err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			} else if err != nil {
				log.Error("error redis log", "redis_caller", new(Utils).FileWithLineNum(), err, err.Error(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			// 慢日志
			if conn.conf.SlowThreshold > 0 && cost > time.Duration(conn.conf.SlowThreshold)*time.Millisecond {
				log.Error("slow redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
				return
			}

			if conn.conf.LogMode {
				log.Debug("debug redis log", "redis_caller", new(Utils).FileWithLineNum(), "addr", conn.conf.DSN, "cmd", s, "cost", cost.String())
			}

			return
		}
	}
}

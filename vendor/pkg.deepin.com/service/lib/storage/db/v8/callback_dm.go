package db

import (
	"bytes"
	"reflect"
	"sort"
	"strings"

	"github.com/jinzhu/gorm"
)

// RegisterCallback 回调注册
func RegisterCallback(conn *Conn) {
	conn.DB.Callback().Update().Before("gorm:assign_updating_attributes").Register("dm8_assign_updating_attr_before", func(scope *gorm.Scope) {
		convertSelectsField(scope)
	})

	conn.DB.Callback().Update().After("gorm:assign_updating_attributes").Register("dm8_assign_updating_attr_after", func(scope *gorm.Scope) {
		rebackSelectsField(scope)
	})

	conn.DB.Callback().Update().Before("gorm:save_before_associations").Register("dm8_save_attr_before", func(scope *gorm.Scope) {
		convertSelectsField(scope)
	})

	conn.DB.Callback().Update().After("gorm:save_before_associations").Register("dm8_save_attr_after", func(scope *gorm.Scope) {
		rebackSelectsField(scope)
	})

	conn.DB.Callback().Query().Before("gorm:query").Register("dm8_query_before", func(scope *gorm.Scope) {
		convertField(scope)
	})

	conn.DB.Callback().RowQuery().Before("gorm:row_query").Register("dm8_row_query_before", func(scope *gorm.Scope) {
		convertField(scope)
	})

	conn.DB.Callback().Create().Before("gorm:create").Register("dm8_create_before", func(scope *gorm.Scope) {
		convertField(scope)
		checkKeyWorld(scope)
	})
	conn.DB.Callback().Create().After("gorm:create").Register("dm8_create_after", func(scope *gorm.Scope) {
		rebackKeyWorld(scope)
	})
	conn.DB.Callback().Update().Before("gorm:update").Register("dm8_update_before", func(scope *gorm.Scope) {
		convertField(scope)
		checkColumnKeyWord(scope)
	})
	conn.DB.Callback().Delete().Before("gorm:delete").Register("dm8_delete_before", func(scope *gorm.Scope) {
		convertField(scope)
	})

	conn.DB.Callback().Query().After("gorm:query").Register("dm8_query_after", func(scope *gorm.Scope) {
		convertDataFormat(scope)
	})

	conn.DB.Callback().Query().After("gorm:row_query").Register("dm8_row_query_after", func(scope *gorm.Scope) {
		convertDataFormat(scope)
	})
}

// RegisterCallbackWithGorm 回调注册
func RegisterCallbackWithGorm(db *gorm.DB) {
	db.Callback().Update().Before("gorm:assign_updating_attributes").Register("dm8_assign_updating_attr_before", func(scope *gorm.Scope) {
		convertSelectsField(scope)
	})

	db.Callback().Update().After("gorm:assign_updating_attributes").Register("dm8_assign_updating_attr_after", func(scope *gorm.Scope) {
		rebackSelectsField(scope)
	})

	db.Callback().Update().Before("gorm:save_before_associations").Register("dm8_save_attr_before", func(scope *gorm.Scope) {
		convertSelectsField(scope)
	})

	db.Callback().Update().After("gorm:save_before_associations").Register("dm8_save_attr_after", func(scope *gorm.Scope) {
		rebackSelectsField(scope)
	})

	db.Callback().Query().Before("gorm:query").Register("dm8_query_before", func(scope *gorm.Scope) {
		convertField(scope)
	})

	db.Callback().RowQuery().Before("gorm:row_query").Register("dm8_row_query_before", func(scope *gorm.Scope) {
		convertField(scope)
	})

	db.Callback().Create().Before("gorm:create").Register("dm8_create_before", func(scope *gorm.Scope) {
		convertField(scope)
		checkKeyWorld(scope)
	})
	db.Callback().Create().After("gorm:create").Register("dm8_create_after", func(scope *gorm.Scope) {
		rebackKeyWorld(scope)
	})

	db.Callback().Update().Before("gorm:update").Register("dm8_update_before", func(scope *gorm.Scope) {
		convertField(scope)
		checkColumnKeyWord(scope)
	})
	db.Callback().Delete().Before("gorm:delete").Register("dm8_delete_before", func(scope *gorm.Scope) {
		convertField(scope)
	})

	db.Callback().Query().After("gorm:query").Register("dm8_query_after", func(scope *gorm.Scope) {
		convertDataFormat(scope)
	})

	db.Callback().Query().After("gorm:row_query").Register("dm8_row_query_after", func(scope *gorm.Scope) {
		convertDataFormat(scope)
	})
}

func convertField(scope *gorm.Scope) {
	if scope != nil {
		for _, field := range scope.Fields() {
			field.DBName = convertName(field.DBName)
		}
	}
}

func checkColumnKeyWord(scope *gorm.Scope) {
	if updateAttrs, ok := scope.InstanceGet("gorm:update_attrs"); ok {
		// Sort the column names so that the generated SQL is the same every time.
		updateMap := updateAttrs.(map[string]interface{})
		var columns []string
		for c := range updateMap {
			for _, keyword := range dmKeywords {
				if c == keyword {
					columns = append(columns, c)
					break
				}
			}
		}
		sort.Strings(columns)
		for _, column := range columns {
			value := updateMap[column]
			newKey := "\"" + column + "\""
			updateMap[newKey] = value
			delete(updateMap, column)
		}
		scope.InstanceSet("gorm:update_attrs", updateMap)

	}
}

func checkKeyWorld(scope *gorm.Scope) {
	if scope != nil {
		for _, field := range scope.Fields() {
			for _, keyword := range dmKeywords {
				if field.DBName == keyword {
					field.DBName = "\"" + field.DBName + "\""
					break
				}
			}
		}
	}
}

func rebackKeyWorld(scope *gorm.Scope) {
	if scope != nil {
		for _, field := range scope.Fields() {
			for _, keyword := range dmKeywords {
				if field.DBName == "\""+keyword+"\"" {
					field.DBName = strings.ReplaceAll(field.DBName, "\"", "")
					break
				}
			}
		}
	}
}

func convertDataFormat(scope *gorm.Scope) {
	if scope != nil {

	}
}

func convertSelectsField(scope *gorm.Scope) {
	if scope != nil {
		for _, field := range scope.Fields() {
			field.DBName = defaultNamer(field.DBName)
		}
	}
}

func rebackSelectsField(scope *gorm.Scope) {
	if scope != nil {
		for _, field := range scope.Fields() {
			field.DBName = convertName(field.DBName)
		}
	}
}

func convertName(name string) string {
	const (
		lower = false
		upper = true
	)

	if v := sMap.Get(name); v != "" {
		return v
	}

	if name == "" {
		return ""
	}

	// 如果已经转换为大写了就直接返回，不需要再转换一次，容易导致有多个下划线产生
	if name == strings.ToUpper(name) {
		return name
	}

	var (
		value                                    = commonInitialismReplacer.Replace(name)
		buf                                      = bytes.NewBufferString("")
		lastCase, currCase, nextCase, nextNumber bool
	)

	for i, v := range value[:len(value)-1] {
		nextCase = bool(value[i+1] >= 'A' && value[i+1] <= 'Z')
		nextNumber = bool(value[i+1] >= '0' && value[i+1] <= '9')

		if i > 0 {
			if currCase == upper {
				if lastCase == upper && (nextCase == upper || nextNumber == upper) {
					buf.WriteRune(v)
				} else {
					if value[i-1] != '_' && value[i+1] != '_' {
						buf.WriteRune('_')
					}
					buf.WriteRune(v)
				}
			} else {
				buf.WriteRune(v)
				if i == len(value)-2 && (nextCase == upper && nextNumber == lower) {
					buf.WriteRune('_')
				}
			}
		} else {
			currCase = upper
			buf.WriteRune(v)
		}
		lastCase = currCase
		currCase = nextCase
	}

	buf.WriteByte(value[len(value)-1])

	s := strings.ToUpper(buf.String())
	sMap.Set(name, s)
	return s
}

func indirect(reflectValue reflect.Value) reflect.Value {
	for reflectValue.Kind() == reflect.Ptr {
		reflectValue = reflectValue.Elem()
	}
	return reflectValue
}

func defaultNamer(name string) string {
	const (
		lower = false
		upper = true
	)

	if v := sMap.Get(name); v != "" {
		return v
	}

	if name == "" {
		return ""
	}

	// 如果已经转换为大写了就直接返回，不需要再转换一次，容易导致有多个下划线产生
	if name == strings.ToUpper(name) {
		// @TODO 取消小写转换，使用大写字段，兼容达梦数据库CreatedAt、UpdatedAt等时间字段
		// return strings.ToLower(name)
		return name
	}

	var (
		value                                    = commonInitialismReplacer.Replace(name)
		buf                                      = bytes.NewBufferString("")
		lastCase, currCase, nextCase, nextNumber bool
	)

	for i, v := range value[:len(value)-1] {
		nextCase = bool(value[i+1] >= 'A' && value[i+1] <= 'Z')
		nextNumber = bool(value[i+1] >= '0' && value[i+1] <= '9')

		if i > 0 {
			if currCase == upper {
				if lastCase == upper && (nextCase == upper || nextNumber == upper) {
					buf.WriteRune(v)
				} else {
					if value[i-1] != '_' && value[i+1] != '_' {
						buf.WriteRune('_')
					}
					buf.WriteRune(v)
				}
			} else {
				buf.WriteRune(v)
				if i == len(value)-2 && (nextCase == upper && nextNumber == lower) {
					buf.WriteRune('_')
				}
			}
		} else {
			currCase = upper
			buf.WriteRune(v)
		}
		lastCase = currCase
		currCase = nextCase
	}

	buf.WriteByte(value[len(value)-1])

	// s := strings.ToLower(buf.String())
	s := strings.ToUpper(buf.String())
	sMap.Set(name, s)
	return s
}

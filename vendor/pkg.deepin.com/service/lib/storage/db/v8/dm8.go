package db

import (
	"github.com/jinzhu/gorm"
	"strings"
)

var schemaName string
var commonInitialism = []string{"API", "ASCII", "CPU", "CSS", "DNS", "EOF", "GUID", "HTML", "HTTP", "HTTPS", "ID", "IP", "JSON", "LHS", "QPS", "RAM", "RHS", "RPC", "SLA", "SMTP", "SSH", "TLS", "TTL", "UID", "UI", "UUID", "URI", "URL", "UTF8", "VM", "XML", "XSRF", "XSS"}
var dmKeywords = []string{"ROLE", "DESC", "role", "desc"}
var sMap = newSafeMap()
var commonInitialismReplacer *strings.Replacer

// InitDM8 init dm8 config & callback
func InitDM8(schema string)  {
	schemaName = schema
	
	var commonInitialismForReplacer []string
	for _, initialism := range commonInitialism {
		commonInitialismForReplacer = append(commonInitialismForReplacer, initialism, strings.Title(strings.ToLower(initialism)))
	}
	commonInitialismReplacer = strings.NewReplacer(commonInitialismForReplacer...)

	gorm.AddNamingStrategy(dmNamingStrategy)

	gorm.DefaultCallback.Create().After("gorm:begin_transaction").Register("dm:set_identity_insert", setIdentityInsert)
	gorm.DefaultCallback.Create().Before("gorm:create").Register("dm:set_identity_insert", setIdentityInsert)
	gorm.DefaultCallback.Create().Before("gorm:commit_or_rollback_transaction").Register("dm:turn_off_identity_insert", turnOffIdentityInsert)
}

func loadAndWriteFile()  {
	
}

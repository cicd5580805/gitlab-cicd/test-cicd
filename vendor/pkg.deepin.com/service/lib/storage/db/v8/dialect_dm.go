package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var keyNameRegex = regexp.MustCompile("[^a-zA-Z0-9]+")

// DefaultForeignKeyNamer contains the default foreign key name generator method
type DefaultForeignKeyNamer struct {
}

// CommonDialect CommonDialect
type CommonDialect struct {
	db gorm.SQLCommon
	DefaultForeignKeyNamer
}

func init() {
	gorm.RegisterDialect("dm", &CommonDialect{})
}

// GetName GetName
func (CommonDialect) GetName() string {
	return "dm"
}

// SetSchema SetSchema
func (s *CommonDialect) SetSchema(schema string) error {
	_, err := s.db.Exec(fmt.Sprintf("SET SCHEMA %v", schema))
	return err
}

// SetDB SetDB
func (s *CommonDialect) SetDB(db gorm.SQLCommon) {
	s.db = db
}

// BindVar BindVar
func (CommonDialect) BindVar(i int) string {
	return "$$$" // ?
}

// Quote Quote
func (CommonDialect) Quote(key string) string {
	key = fmt.Sprintf(`"%s"`, key)
	return strings.ToUpper(key)
	//return fmt.Sprintf(`"%s"`, key)
}

func (s *CommonDialect) fieldCanAutoIncrement(field *gorm.StructField) bool {
	if value, ok := field.TagSettingsGet("AUTO_INCREMENT"); ok {
		return strings.ToLower(value) != "false"
	}
	return field.IsPrimaryKey
}

// DataTypeOf DataTypeOf
func (s *CommonDialect) DataTypeOf(field *gorm.StructField) string {
	field.TagSettingsDelete("COMMENT")
	var dataValue, sqlType, size, additionalType = gorm.ParseFieldStructForDialect(field, s)
	//var test = &dm.DmClob{}
	//len, err := test.GetLength()
	//if err != nil {
	//	return ""
	//}
	//
	//test.ReadString(0, int(len))
	if sqlType == "" {
		switch dataValue.Kind() {
		case reflect.Bool:
			sqlType = "BOOLEAN"
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uintptr:
			if s.fieldCanAutoIncrement(field) {
				sqlType = "INTEGER IDENTITY(1, 1)"
			} else {
				sqlType = "INTEGER"
			}
		case reflect.Int64, reflect.Uint64:
			if s.fieldCanAutoIncrement(field) {
				sqlType = "BIGINT IDENTITY(1, 1)"
			} else {
				sqlType = "BIGINT"
			}
		case reflect.Float32, reflect.Float64:
			sqlType = "FLOAT"
		case reflect.String:
			if size > 0 && size < 65532 {
				sqlType = fmt.Sprintf("VARCHAR(%d)", size)
			} else {
				sqlType = "VARCHAR(65532)"
			}
		case reflect.Struct:
			if _, ok := dataValue.Interface().(time.Time); ok {
				sqlType = "TIMESTAMP"
			}
		default:
			if _, ok := dataValue.Interface().([]byte); ok {
				if size > 0 && size < 65532 {
					sqlType = fmt.Sprintf("BLOB(%d)", size)
				} else {
					sqlType = "BLOB(65532)"
				}
			}
		}
	} else {
		if strings.Contains(sqlType, "int(") {
			sqlType = "INT"
		}
	}

	if sqlType == "" {
		panic(fmt.Sprintf("invalid sql type %s (%s) for commonDialect", dataValue.Type().Name(), dataValue.Kind().String()))
	}

	if strings.TrimSpace(additionalType) == "" {
		return sqlType
	}
	return fmt.Sprintf("%v %v", sqlType, additionalType)
}

// HasIndex HasIndex
func (s CommonDialect) HasIndex(tableName string, indexName string) bool {
	var count int
	currentDatabase, tableName := currentDatabaseAndTable(&s, tableName)
	s.db.QueryRow("SELECT count(*) FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema = ? AND table_name = ? AND index_name = ?", currentDatabase, tableName, indexName).Scan(&count)
	return count > 0
}

// RemoveIndex RemoveIndex
func (s CommonDialect) RemoveIndex(tableName string, indexName string) error {
	_, err := s.db.Exec(fmt.Sprintf("DROP INDEX %v", indexName))
	return err
}

// HasForeignKey HasForeignKey
func (s CommonDialect) HasForeignKey(tableName string, foreignKeyName string) bool {
	return false
}

// HasTable HasTable
func (s CommonDialect) HasTable(tableName string) bool {
	var count int
	currentDatabase, tableName := currentDatabaseAndTable(&s, tableName)
	err := s.db.QueryRow(fmt.Sprintf("select * from %v.%v limit 0, 1", currentDatabase, tableName)).Scan(&count)
	//_, err := s.db.QueryRow(fmt.Sprintf("select * from %v.%v limit 0, 1", currentDatabase, tableName))
	if err != nil && err.Error() != ErrRecordNotFound.Error() {
		return false
	}
	return true
}

// HasColumn HasColumn
func (s CommonDialect) HasColumn(tableName string, columnName string) bool {
	var count int
	currentDatabase, tableName := currentDatabaseAndTable(&s, tableName)
	s.db.QueryRow("SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = ? AND table_name = ? AND column_name = ?", currentDatabase, tableName, columnName).Scan(&count)
	return count > 0
}

// ModifyColumn ModifyColumn
func (s CommonDialect) ModifyColumn(tableName string, columnName string, typ string) error {
	_, err := s.db.Exec(fmt.Sprintf("ALTER TABLE %v ALTER COLUMN %v TYPE %v", tableName, columnName, typ))
	return err
}

// CurrentDatabase dm这里要获取scheme
func (s CommonDialect) CurrentDatabase() (name string) {
	//s.db.QueryRow("SELECT DATABASE()").Scan(&name)
	return schemaName
}

// LimitAndOffsetSQL return generated SQL with Limit and Offset
func (s CommonDialect) LimitAndOffsetSQL(limit, offset interface{}) (sql string, err error) {
	if limit != nil {
		if parsedLimit, err := s.parseInt(limit); err != nil {
			return "", err
		} else if parsedLimit >= 0 {
			sql += fmt.Sprintf(" LIMIT %d", parsedLimit)
		}
	}
	if offset != nil {
		if parsedOffset, err := s.parseInt(offset); err != nil {
			return "", err
		} else if parsedOffset >= 0 {
			sql += fmt.Sprintf(" OFFSET %d", parsedOffset)
		}
	}
	return
}

//SelectFromDummyTable SelectFromDummyTable
func (CommonDialect) SelectFromDummyTable() string {
	return ""
}

// LastInsertIDOutputInterstitial LastInsertIDOutputInterstitial
func (CommonDialect) LastInsertIDOutputInterstitial(tableName, columnName string, columns []string) string {
	return ""
}

// LastInsertIDReturningSuffix LastInsertIDReturningSuffix
func (CommonDialect) LastInsertIDReturningSuffix(tableName, columnName string) string {
	return ""
}

// DefaultValueStr DefaultValueStr
func (CommonDialect) DefaultValueStr() string {
	return "DEFAULT VALUES"
}

// BuildKeyName returns a valid key name (foreign key, index key) for the given table, field and reference
func (DefaultForeignKeyNamer) BuildKeyName(kind, tableName string, fields ...string) string {
	keyName := fmt.Sprintf("%s_%s_%s", kind, tableName, strings.Join(fields, "_"))
	keyName = keyNameRegex.ReplaceAllString(keyName, "_")
	return keyName
}

// NormalizeIndexAndColumn returns argument's index name and column name without doing anything
func (CommonDialect) NormalizeIndexAndColumn(indexName, columnName string) (string, string) {
	return indexName, columnName
}

func (CommonDialect) parseInt(value interface{}) (int64, error) {
	return strconv.ParseInt(fmt.Sprint(value), 0, 0)
}

// IsByteArrayOrSlice returns true of the reflected value is an array or slice
func IsByteArrayOrSlice(value reflect.Value) bool {
	return (value.Kind() == reflect.Array || value.Kind() == reflect.Slice) && value.Type().Elem() == reflect.TypeOf(uint8(0))
}

func currentDatabaseAndTable(dialect gorm.Dialect, tableName string) (string, string) {
	if strings.Contains(tableName, ".") {
		splitStrings := strings.SplitN(tableName, ".", 2)
		return splitStrings[0], splitStrings[1]
	}
	return dialect.CurrentDatabase(), tableName
}

func setIdentityInsert(scope *gorm.Scope) {
	if scope.Dialect().GetName() == "dm" {
		for _, field := range scope.PrimaryFields() {
			if _, ok := field.TagSettingsGet("AUTO_INCREMENT"); ok && !field.IsBlank {
				scope.NewDB().Exec(fmt.Sprintf("SET IDENTITY_INSERT %v ON", scope.TableName()))
				scope.InstanceSet("dm:identity_insert_on", true)
			}
		}
	}
}

func turnOffIdentityInsert(scope *gorm.Scope) {
	if scope.Dialect().GetName() == "dm" {
		if _, ok := scope.InstanceGet("dm:identity_insert_on"); ok {
			scope.NewDB().Exec(fmt.Sprintf("SET IDENTITY_INSERT %v OFF", scope.TableName()))
		}
	}
}

package db

import (
	"github.com/jinzhu/gorm"
)

var dmNamingStrategy = &gorm.NamingStrategy{
	DB:     dmNamer,
	Table:  tableNamer,
	Column: columnNamer,
}

func dmNamer(name string) string {
	return convertName(name)
}

func tableNamer(name string) string {
	return convertName(name)
}

func columnNamer(name string) string  {
	column := convertName(name)
	return column
}
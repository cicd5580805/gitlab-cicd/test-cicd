.PHONY : run build docker docker-release
COMMIT_ID := $(shell git rev-parse --short HEAD)
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
# 当前所在路径
CURDIR := $(shell pwd)
# 入口 main.go 所在文件夹路径
MAIN_PATH := $(if $(MAIN_PATH),$(MAIN_PATH),$(CURDIR))
# 可执行文件名称
APP_NAME := $(if $(APP_NAME),$(APP_NAME),local)
# 执行环境（通过应用中心发布时会自动写入）hang
TARGET := $(if $(TARGET),$(TARGET),local)
# 平台架构
ARCH := $(if $(ARCH),$(ARCH),linux/amd64)
DOCKER_ARCH := $(shell echo $(ARCH)|awk '{ sub(/\//,"-"); print $$0 }')
# 服务名称
PROJECT_NAME := $(shell echo $(CURDIR)|awk -F '/' '{ print $$NF }')
# 仓库地址（通过应用中心发布时会自动写入）
DOCKER_BASE := $(if $(DOCKER_BASE),$(DOCKER_BASE),hub.deepin.com/wuhan_udcp)
# 镜像标签（通过应用中心发布时会自动写入）
IMAGE_STUFF := $(if $(IMAGE_STUFF),$(IMAGE_STUFF),$(DOCKER_ARCH))
# 指定编译哪些模块，不填则默认编译 app 目录下所有模块
BUILD_APPS := $(if $(BUILD_APPS),$(BUILD_APPS),$(shell ls $(CURDIR)/app))
# 打包时间
BUILD_DATE := $(shell date "+%Y-%m-%d/%H:%M:%S")
LDFLAGS := "-w -s -extldflags -static -X github.com/gotomicro/ego/core/eapp.appName='${APP_NAME}' -X github.com/gotomicro/ego/core/eapp.buildVersion='${COMMIT_ID}' -X github.com/gotomicro/ego/core/eapp.buildAppVersion='${COMMIT_ID}' -X github.com/gotomicro/ego/core/eapp.buildStatus='Modified' -X github.com/gotomicro/ego/core/eapp.buildTag='${TARGET}' -X github.com/gotomicro/ego/core/eapp.buildUser='doraemon' -X github.com/gotomicro/ego/core/eapp.buildHost='127.0.0.1' -X github.com/gotomicro/ego/core/eapp.buildTime='${BUILD_DATE}'"
# go mod，从传入取值，建议为mod，为兼容历史版本，默认为vendor
GO_MOD := $(if $(GO_MOD),$(GO_MOD),vendor)

$(info COMMIT_ID=$(COMMIT_ID))
$(info BRANCH=$(BRANCH))
$(info CURDIR=$(CURDIR))
$(info MAIN_PATH=$(MAIN_PATH))
$(info APP_NAME=$(APP_NAME))
$(info TARGET=$(TARGET))
$(info ARCH=$(ARCH))
$(info DOCKER_ARCH=$(DOCKER_ARCH))
$(info PROJECT_NAME=$(PROJECT_NAME))
$(info DOCKER_BASE=$(DOCKER_BASE))
$(info IMAGE_STUFF=$(IMAGE_STUFF))
$(info BUILD_DATE=$(BUILD_DATE))
$(info BUILD_APPS=$(BUILD_APPS))
$(info GO_MOD=$(GO_MOD))
$(info ----------)

# 本地执行，make build, 默认使用vendor; 不使用vendor, 执行 make build GO_MOD=mod
.PHONY: build
build:
	for i in $(BUILD_APPS) ; do \
		GOARCH=$(shell echo $(ARCH) | cut -d "/" -f 2) CGO_ENABLED=0 go build -mod=$(GO_MOD) -ldflags $(LDFLAGS) -o $(CURDIR)/app/$$i/$$i $(CURDIR)/app/$$i/main.go || exit 1 ; \
	done

# 通过ARCH指定镜像架构，默认为linux/amd64，通过IMAGE_STUFF指定镜像标签，默认为linux-amd64
# 如果要编译amd，标签为V1.0.1-dev，执行 make docker ARCH=linux/amd64 IMAGE_STUFF=V1.0.1-dev
# 如果要编译arm，标签为V1.0.1-dev，执行 make docker ARCH=linux/arm64 IMAGE_STUFF=V1.0.1-dev
# 如果要编译mips，标签为V1.0.1-dev，执行 make docker ARCH=linux/mips64le IMAGE_STUFF=V1.0.1-dev
.PHONY: docker
docker:copy
	for i in $(BUILD_APPS) ; do \
		docker buildx build \
			--build-arg PROJECT_NAME=$(PROJECT_NAME) \
			--build-arg BRANCH=$(BRANCH) \
			--build-arg COMMIT_ID=$(COMMIT_ID) \
			--build-arg APP_VERSION=$(IMAGE_STUFF) \
			--build-arg BUILD_DATE=$(BUILD_DATE) \
			--build-arg TARGET=$(TARGET) \
			--build-arg ARCH=$(ARCH) \
			--build-arg APP_NAME=$$i \
			--build-arg GITLAB_PROJECT_ID=$(GITLAB_PROJECT_ID) \
			--build-arg MAIN_PATH=app/$$i \
			--build-arg GO_MOD=$(GO_MOD) \
			--pull \
			--push \
			--platform=$(ARCH) \
			-t $(DOCKER_BASE)/$(PROJECT_NAME)/$$i:$(IMAGE_STUFF) \
			-f Dockerfile . || exit 1 ; \
	done

.PHONY: copy
copy:
	for i in `ls $(CURDIR)/app` ; do \
  		if [ ! -e $(CURDIR)/app/$$i/data ]; then mkdir $(CURDIR)/app/$$i/data; fi \
	done


# 代码格式化检查
.PHONY: gofmt
gofmt:
	@echo "代码格式化检查"
	@file_num=$(shell gofmt -l $(shell find app/ -name "*.go") | wc -l) && \
	if [ $$file_num -gt 0 ]; \
	then echo "代码未全部格式化，请执行命令: find app/ -name '*.go' | xargs gofmt -l -w -s; 将代码格式化" && exit 1 ; fi


# 静态代码检查
.PHONY: lint
lint:
	@echo "静态代码检查"
	#1.需要安装golangci-lint，参考 https://golangci-lint.run/usage/install/
	#golangci-lint version
	#golangci-lint run -v
	#2.以docker运行
	docker run -t --rm --network=host -u root -v $(CURDIR):/app -w /app $(DOCKER_BASE)/golangci/golangci-lint:v1.52.0 sh -c "ls -lha /app; golangci-lint run --config ./.golangci-lint.yml -v"


# 执行单元测试
.PHONY: test
test:
	#CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go test -gcflags="all=-N -l" -mod=$(GO_MOD) ./... -count=1 -v -coverprofile=cover.out
	go test -gcflags="all=-N -l" ./... -count=1 -v -coverprofile=cover.out

# 生成单元测试覆盖率文件
.PHONY: unit_test_cover_file
unit_test_cover_file: test
	go tool cover -html=cover.out -o cover.html
	go tool cover -func=cover.out -o cover.txt

# 输出单元测试覆盖率  # shell命令会先于makefile命令执行，需要将单元测试文件作为依赖，否则cover.txt文件不存在
.PHONY: unit_test
unit_test: unit_test_cover_file
	@coverage=$(shell grep "total:" cover.txt | awk '{print $$NF}') && echo "total coverage: $$coverage"

